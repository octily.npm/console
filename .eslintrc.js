module.exports = {
  extends: ['eslint:recommended', 'react-app', 'react-app/jest'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    trackJs: 'readonly'
  },
  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    indent: 0,
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'always']
  },
  ignorePatterns: ['src/i18n/languages/*']
};
