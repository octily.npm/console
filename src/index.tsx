export function octilyInfo(): void {
    const octilyLogging = window.octilyLogging;
    const currentYear: number = new Date().getFullYear();
    const line1: string = `🐙 Customization by Octily © 2016-${currentYear} Octily GmbH. All rights reserved. https://octily.com • support@octily.com`;

    const logMessage: string = `%c${line1}`;
    const logStyle: string = 'background: #0AC147; padding: 20px; color: #ffffff; text-shadow: 2px 2px rgba(0,0,0,0.2); font-size: 10pt; border-radius: 10px;';

    if (!octilyLogging) {
        console.log(logMessage, logStyle);
        window.octilyLogging = true;
    }
}